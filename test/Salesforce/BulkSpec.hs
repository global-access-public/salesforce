{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wno-unticked-promoted-constructors #-}

module Salesforce.BulkSpec where

import Data.CSV.Product (
  L (E, L, T),
  Op (NoOp, PutOp, UpdateOp),
  RC (RC),
  defRT,
 )
import Data.Map.Strict qualified as Map
import Data.Tagged (Tagged (Tagged), untag)
import Generics.SOP (I (..), NP (Nil, (:*)))
import Generics.SOP.TH (deriveGeneric)
import Protolude
import Salesforce.API (IndexOf)
import Salesforce.Bulk (bulkDelete, bulkInsert, bulkQueryS, bulkUpdate)
import Salesforce.Login (CallSFT (CallSFT), WithSF, callSFY, getSFConfig, SfConf(sfUrl))
import Servant.Client (BaseUrl (..))
import Servant.CSV.Records (CSVMulti (..))
import Streaming.Prelude qualified as S
import Test.Hspec (SpecWith, describe, it, shouldBe)

newtype AccountName = AccountName Text deriving (Show, Eq, Ord)

deriveGeneric ''AccountName

data Account = Account (IndexOf "Account") AccountName
  deriving (Show)

instance CSVMulti AccountName where
  type CSVIndex AccountName = IndexOf "Account"
  type CSVProducts AccountName = '[AccountName]
  csvMulti _ =
    T (defRT "Id") $ L (RC $ defRT "Name" :* Nil) E

getAccount ::
  L (UpdateOp (PutOp NoOp)) I (IndexOf "Account") '[AccountName] ->
  Account
getAccount (T (I i) (L (I a) E)) = Account i a

getAccountName ::
  L ( 'PutOp 'NoOp) I (IndexOf "Account") '[AccountName] ->
  AccountName
getAccountName (L (I n) E) = n

extractIndex :: Account -> L ( 'UpdateOp 'NoOp) I (IndexOf "Account") '[]
extractIndex (Account id _) = T (I id) E

newAccountName :: Text -> L ( 'PutOp 'NoOp) I o '[AccountName]
newAccountName x = L (I $ AccountName x) E

-- updateAccountName :: Text -> Account -> UpdateL Account
updateAccountName ::
  Text ->
  IndexOf "Account" ->
  L ('UpdateOp ('PutOp 'NoOp))
                    I
                    (IndexOf "Account")
                    '[AccountName]
updateAccountName x id = T (I id) (L (I (AccountName x)) E)

testCredentials :: FilePath
testCredentials = "/secrets/test/salesforce.json"

openTestSalesforce :: IO (CallSFT IO)
openTestSalesforce = do
  r <- getSFConfig testCredentials
  when
    do baseUrlHost (sfUrl r) /= "globalaccess--accounting.sandbox.my.salesforce.com"
    do panic "not sure it's a sandbox credentials"
  callSFY testCredentials

withAccountingSandbox :: WithSF b -> IO b
withAccountingSandbox f = do
  CallSFT run <- openTestSalesforce
  run f

spec :: SpecWith ()
spec = do
  describe "Login" $ do
    it "can connect" $ do
      withAccountingSandbox $ pure ()
    it "can bulk query accounts" $ do
      l <- withAccountingSandbox $ do
        S.length_
          . S.map (getAccount . untag @AccountName . untag @"Account")
          $ bulkQueryS 10000 ""
      l >= 0 `shouldBe` True
    it "can bulk delete accounts" $ do
      withAccountingSandbox $ do
        accounts <-
          S.toList_
            . S.map (getAccount . untag @AccountName . untag @"Account")
            $ bulkQueryS 10000 ""
        bulkDelete @AccountName
          (Proxy @"Account")
          (Tagged $ accounts <&> extractIndex)
    it "can bulk insert one account" $ do
      news <- withAccountingSandbox $ do
        bulkInsert @AccountName
          (Proxy @"Account")
          getAccountName
          (Tagged [newAccountName "paolino"])
      length news `shouldBe` 1
    it "can bulk patch one account" $ do
      withAccountingSandbox $ do
        news <-
          bulkInsert @AccountName
            (Proxy @"Account")
            getAccountName
            (Tagged [newAccountName "tobias"])
        let tobiasId = news Map.! AccountName "tobias"
        bulkUpdate @AccountName
          (Proxy @"Account")
          (Tagged [updateAccountName "nikola" tobiasId])

-- it "can bulk insert one account" $ do
--   (_, news) <- withAccountingSandbox $ do
--     bulkInsert @AccountName
--       (Proxy @"Account")
--       (const ())
--       (Tagged [newAccountName "paolino"])
--   null news `shouldBe` True
