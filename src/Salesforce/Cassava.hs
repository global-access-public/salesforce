{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Salesforce.Cassava where

import Control.Monad.Catch (MonadMask, catchAll)
import Control.Monad.Fail
import Control.Monad.Trans.Resource
  ( MonadResource
  , MonadUnliftIO
  , ResourceT
  , runResourceT
  )
import Data.CSV.Product (P (P), R (R), RT (RT))
import Data.Csv (DefaultOrdered, FromNamedRecord, ToNamedRecord)
import Data.Logger (Logger)
import Data.Time
  ( Day
  , UTCTime
  , defaultTimeLocale
  , formatTime
  , parseTimeM
  )
import Data.UUID as U (UUID, fromText, toText)
import Protolude hiding (toS)
import Protolude.Conv (toS)
import Salesforce.Login (CallSFT (CallSFT), WithSF)
import Streaming (MFunctor (hoist), MonadTrans, Of, Stream)
import qualified Streaming.ByteString.Char8 as Q
import Streaming.Cassava
  ( CsvParseException
  , decodeByName
  , encodeByNameDefault
  )

boolRT :: Text -> RT Bool
boolRT c = RT
  c
  do
    R $ \case
      True -> "true"
      False -> "false"
  do
    P $ \case
      "true" -> pure True
      "false" -> pure False
      _ -> fail "can't parse a bool"

utcRT :: Text -> RT UTCTime
utcRT c = RT
  c
  do R $ toS . formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S%QZ"
  do P $ parseTimeM True defaultTimeLocale "%Y-%m-%dT%H:%M:%S%QZ" . toS

dayRT :: Text -> RT Day
dayRT c = RT
  c
  do R $ toS . formatTime defaultTimeLocale "%Y-%m-%d"
  do P $ parseTimeM True defaultTimeLocale "%Y-%m-%d" . toS

uuidRT :: Text -> RT UUID
uuidRT c = RT
  c
  do R $ toS . U.toText
  do
    P $ \x -> case U.fromText . toS $ x of
      Nothing -> fail "cannot parse UUID"
      Just uuid -> pure uuid

saveNamedCSV
  :: ( MonadResource m
     , DefaultOrdered a
     , ToNamedRecord a
     )
  => FilePath
  -> Stream (Of a) m r
  -> m r
saveNamedCSV fp = Q.writeFile fp . encodeByNameDefault

saveSalesforceRecords
  :: ( MonadUnliftIO m
     , DefaultOrdered a
     , ToNamedRecord a
     )
  => CallSFT m
  -> FilePath
  -> Stream (Of a) WithSF r
  -> m r
saveSalesforceRecords (CallSFT callSF) fp =
  runResourceT . saveNamedCSV fp
    . hoist (lift . callSF)

readNamedCSV
  :: ( MonadError CsvParseException (t m)
     , MonadResource m
     , MonadTrans t
     , FromNamedRecord a
     )
  => FilePath
  -> Stream (Of a) (t m) ()
readNamedCSV fp = decodeByName $ hoist lift $ Q.readFile fp

type CSVE m = ExceptT CsvParseException (ResourceT m)

withCSVRecords
  :: (MonadUnliftIO m, FromNamedRecord a)
  => FilePath
  -> (Stream (Of a) (CSVE m) () -> CSVE m r)
  -> m (Either CsvParseException r)
withCSVRecords fp f = runResourceT $ runExceptT $ f $ readNamedCSV fp

newtype CachingLog = CachingException SomeException deriving (Show)

withCachedValues
  :: ( MonadMask m
     , MonadUnliftIO m
     , DefaultOrdered a
     , ToNamedRecord a
     , FromNamedRecord a
     )
  => CallSFT m
  -> Logger CachingLog
  -> FilePath
  -> Stream (Of a) WithSF ()
  -> (Stream (Of a) (CSVE m) () -> CSVE m r)
  -> m (Either CsvParseException r)
withCachedValues csf logger fp getValues f = do
  catchAll
    do void $ saveSalesforceRecords csf fp getValues
    do logger . CachingException
  withCSVRecords fp f
