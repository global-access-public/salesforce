{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-unticked-promoted-constructors #-}

module Salesforce.Streaming.Sync where

import Data.CSV.Product
  ( L (E, L, T)
  , ToLie
  , lie
  , lieFromProduct
  , ti
  , tie, Op (..)
  )
import Data.Dependent.Sum (DSum (..))
import Data.Logger (Logger)
import Data.Tagged (Tagged (Tagged))
import Generics.SOP
  ( I (..)
  , IsProductType
  , All
  )
import Protolude hiding (All)
import Salesforce.API (IndexOf)
import Salesforce.Bulk (bulkDelete, bulkInsert, bulkQueryS, bulkUpdate)
import Salesforce.Login (CallSFT (CallSFT, runSF), WithSF)
import Servant.CSV.Records
  ( CSVMulti (CSVIndex, CSVProducts)
  , CSVSingle (CSVIndexSingle)
  , QueryL
  , Single
  )
import Streaming (hoist)
import qualified Streaming.Prelude as S
import Streaming.Sync (Equality, computeUpdates, computeUpdatesEq)
import Streaming.Sync.Batch (BatchUpdate (..), groupBatch)

getSingleS
  :: forall object value xs.
  ( KnownSymbol object
  , CSVIndexSingle value ~ IndexOf object
  , CSVSingle value
  , IsProductType value xs
  , Typeable value
  )
  => Int
  -> Text
  -> S.Stream (S.Of (Tagged object (Tagged (Single value) (QueryL (Single value))))) WithSF ()
getSingleS = bulkQueryS

pushSingleChanges
  :: forall object value xs.
  ( KnownSymbol object
  , CSVIndexSingle value ~ IndexOf object
  , CSVSingle value
  , IsProductType value xs
  , Typeable value
  , Show value
  )
  => DSum
       (BatchUpdate (IndexOf object) value)
       []
  -> WithSF ()
pushSingleChanges = \case
  BatchDelete :=> as ->
    bulkDelete
      do Proxy @object
      do Tagged @(Single value) $ tie <$> as
  BatchInsert :=> as ->
    void $ bulkInsert
      do Proxy @object
      do const ()
      do Tagged @(Single value) $ lie <$> as
  BatchUpdate :=> as ->
    void $ bulkUpdate
      do Proxy @object
      do Tagged @(Single value) $ [T (I q) $ lie v | ((q, _), v) <- as]

pushMultiChanges
  :: forall object value xs.
  ( KnownSymbol object
  , CSVIndex value ~ IndexOf object
  , CSVMulti value
  , CSVProducts value ~ xs
  , IsProductType value xs
  , ToLie xs
  , Typeable value
  , All Show xs
  )
  => DSum
       (BatchUpdate (IndexOf object) value)
       []
  -> WithSF ()
pushMultiChanges = \case
  BatchDelete :=> as ->
    bulkDelete
      do Proxy @object
      do Tagged @value $ tie <$> as
  BatchInsert :=> as ->
    void $ bulkInsert
      do Proxy @object
      do const ()
      do Tagged @value $ lieFromProduct <$> as
  BatchUpdate :=> as ->
    void $ bulkUpdate
      do Proxy @object
      do Tagged @value $ [ti q $ lieFromProduct v | ((q, _), v) <- as]

updateSalesforceMulti
  :: ( CSVMulti value
     , KnownSymbol object
     , ToLie (CSVProducts value)
     , CSVProducts value ~ xs
     , Ord a
     , Eq value
     , CSVIndex value ~ IndexOf object
     , IsProductType value xs
     , Typeable value
     , All Show xs
     )
  => Logger (DSum (BatchUpdate (IndexOf object) value) [])
  -> S.Stream (S.Of (a, (IndexOf object, value))) WithSF r
  -> S.Stream (S.Of (a, value)) WithSF s
  -> WithSF (r, s)
updateSalesforceMulti logger x y =
  S.mapM_ pushMultiChanges . S.chain logger . groupBatch 10000 . S.map snd $ computeUpdates x y

updateSalesforceSingle
  :: ( KnownSymbol object
     , CSVIndexSingle value ~ IndexOf object
     , CSVSingle value
     , Eq value
     , IsProductType value xs
     , Ord a
     , Typeable value
     , Show value
     )
  => Logger (DSum (BatchUpdate (IndexOf object) value) [])
  -> S.Stream (S.Of (a, (IndexOf object, value))) WithSF r
  -> S.Stream (S.Of (a, value)) WithSF s
  -> WithSF (r, s)
updateSalesforceSingle logger = updateSalesforceSingleEq logger (==)

updateSalesforceSingleEq
  :: ( KnownSymbol object
     , CSVIndexSingle value ~ IndexOf object
     , CSVSingle value
     , IsProductType value xs
     , Ord a
     , Typeable value
     , Show value
     )
  => Logger (DSum (BatchUpdate (IndexOf object) value) [])
  -> Equality value
  -> S.Stream (S.Of (a, (IndexOf object, value))) WithSF r
  -> S.Stream (S.Of (a, value)) WithSF s
  -> WithSF (r, s)
updateSalesforceSingleEq logger equality x y =
  S.mapM_ pushSingleChanges . S.chain logger . groupBatch 10000 . S.map snd $ computeUpdatesEq equality x y

updateSalesforceSingleEqC
  :: ( KnownSymbol object
     , CSVIndexSingle value ~ IndexOf object
     , CSVSingle value
     , IsProductType value xs
     , Ord a
     , Monad m
     , MonadIO m
     , Typeable value
     , Show value
     )
  => Logger (DSum (BatchUpdate (IndexOf object) value) [])
  -> CallSFT m
  -> Equality value
  -> S.Stream (S.Of (a, (IndexOf object, value))) m r
  -> S.Stream (S.Of (a, value)) m s
  -> m (r, s)
updateSalesforceSingleEqC logger (CallSFT run) equality x y =
  S.mapM_ (run . pushSingleChanges)
    . S.chain logger
    . groupBatch 10000
    . S.map snd
    $ computeUpdatesEq equality x y

updateSalesforceSingleC
  :: ( KnownSymbol object
     , CSVIndexSingle value ~ IndexOf object
     , CSVSingle value
     , IsProductType value xs
     , Ord a
     , Monad m
     , MonadIO m
     , Eq value
     , Typeable value
     , Show value
     )
  => Logger (DSum (BatchUpdate (IndexOf object) value) [])
  -> CallSFT m
  -> S.Stream (S.Of (a, (IndexOf object, value))) m r
  -> S.Stream (S.Of (a, value)) m s
  -> m (r, s)
updateSalesforceSingleC logger run = updateSalesforceSingleEqC logger run (==)

updateSalesforceSingleK
  :: forall object value xs m k s.
  ( KnownSymbol object
  , CSVIndexSingle value ~ IndexOf object
  , CSVSingle value
  , IsProductType value xs
  , Ord k
  , Monad m
  , MonadIO m
  , Eq value
  , Typeable value
  , Show value
  )
  => Logger (DSum (BatchUpdate (IndexOf object) value) []) -- ^
  -> CallSFT m -- ^ how to run SF query
  -> Int -- ^ batch size
  -> Text -- ^ where clause
  -> (value -> k) -- ^ extract key
  -> S.Stream (S.Of (k, value)) m s -- ^ updating signal
  -> m s
updateSalesforceSingleK logger run n s f =
  fmap (fmap snd)
    . updateSalesforceSingleEqC logger run (==)
    . hoist (runSF run)
    . S.map do
      \(Tagged (Tagged (T (I o) (L (I x) E)))) -> (f x, (o, x))
    $ getSingleS @object @value n s

t
  :: (value -> k)
  -> Tagged object (Tagged (Single value) (L ('UpdateOp ('PutOp 'NoOp)) I (IndexOf object) '[value]))
  -> (k, (IndexOf object, value))
t f (Tagged (Tagged (T (I o) (L (I x) E)))) = (f x, (o, x))

-- updateSalesforceSingleEqWith
--   :: ( KnownSymbol object
--      , CSVIndexSingle value ~ IndexOf object
--      , CSVSingle value
--      , IsProductType value xs
--      , Ord a
--      )
--   => (value -> value' -> Bool)
--   -> S.Stream (S.Of (a, (IndexOf object, value))) WithSF r
--   -> S.Stream (S.Of (a, value')) WithSF s
--   -> WithSF (r, s)
-- updateSalesforceSingleEqWith  equality x y =
--   S.mapM_ pushSingleChanges . groupBatch 10000 . S.map snd $ computeUpdatesEq equality x y
