{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Salesforce.Streaming.Query where

import Control.Arrow (left)
import Control.Foldl qualified as L
import Control.Lens
  ( Profunctor (lmap),
    (^..),
    (^?),
  )
import Data.Aeson (Key, Value)
import Data.Aeson.Lens (AsValue (_Array, _String), key)
import Data.JSOP (JSOP, JSOPIssue, jreadS, ksplit)
import Data.Logger (Logger)
import GHC.Generics qualified as GHC
import Generics.SOP (All, I, IsProductType, NP, productTypeTo)
import Protolude hiding (All)
import Salesforce.API (lastSegment, queryP, queryRestP)
import Salesforce.Generic (gquerySOP)
import Salesforce.Login (WithSF, withToken)
import Servant.Client (client)
import Streaming (Of, Stream)
import Streaming.Prelude qualified as S

deriving instance Exception JSOPIssue

-- modifySF
--   :: forall a b xs.
--   (KnownSymbol a, IsProductType b xs, All Typeable xs)
--   => Object a
--   -> NP (JSOP Key) xs
--   -> (b -> WithSF ())
--   -> WithSF ()
-- modifySF object' parsers effect' =
--   S.mapM_
--     do
--       \v -> case jread (T.splitOn ".") parsers v of
--         Left r -> throwM r
--         Right b -> effect' b
--     do queryStreamingSF $ gqueryX (toS $ symbolVal object') (paths parsers) ""

queryStreamingValue :: Text -> Stream (Of Value) WithSF ()
queryStreamingValue v = getStreamingRecords $ withToken $ \tk -> client queryP tk $ Just v

getStreamingRecords :: WithSF Value -> Stream (Of Value) WithSF ()
getStreamingRecords taker = do
  r <- lift taker
  S.each $ r ^.. key "records" . _Array . traverse
  case r ^? key "nextRecordsUrl" . _String of
    Nothing -> pure ()
    Just c -> getStreamingRecords do
      withToken $ \tk -> client queryRestP tk $ lastSegment c

queryStreaming ::
  forall b xs p a.
  (All Typeable xs, KnownSymbol a, IsProductType b xs) =>
  p a ->
  NP (JSOP Key) xs ->
  Text ->
  Stream (Of (Either JSOPIssue b)) WithSF ()
queryStreaming object' query cond = S.map (fmap productTypeTo) $ queryStreamingS object' query cond

queryStreamingS ::
  forall xs p a.
  (All Typeable xs, KnownSymbol a) =>
  p a ->
  NP (JSOP Key) xs ->
  Text ->
  Stream (Of (Either JSOPIssue (NP I xs))) WithSF ()
queryStreamingS object' query cond =
  S.map (jreadS (ksplit ".") query) $ queryStreamingValue (gquerySOP object' query cond)

foldQuery ::
  forall b q xs p a.
  (All Typeable xs, KnownSymbol a, IsProductType b xs) =>
  p a ->
  NP (JSOP Key) xs ->
  Text ->
  L.Fold b q ->
  WithSF q
foldQuery pa ps t f = foldQueryS pa ps t $ lmap productTypeTo f

-- fmap S.fst' $
-- L.purely S.fold f $ S.concat $ queryStreaming pa ps t

foldQueryS ::
  forall q xs p a.
  (All Typeable xs, KnownSymbol a) =>
  p a ->
  NP (JSOP Key) xs ->
  Text ->
  L.Fold (NP I xs) q ->
  WithSF q
foldQueryS pa ps t f =
  fmap S.fst' $
    L.purely S.fold f $ S.concat $ queryStreamingS pa ps t

foldQueryMS ::
  forall q xs p a.
  (All Typeable xs, KnownSymbol a) =>
  p a ->
  NP (JSOP Key) xs ->
  Text ->
  L.FoldM WithSF (NP I xs) q ->
  WithSF q
foldQueryMS pa ps t f =
  fmap S.fst' $
    L.impurely S.foldM f $ S.concat $ queryStreamingS pa ps t

foldQueryM ::
  forall b q xs p a.
  (All Typeable xs, KnownSymbol a, IsProductType b xs) =>
  p a ->
  NP (JSOP Key) xs ->
  Text ->
  L.FoldM WithSF b q ->
  WithSF q
foldQueryM pa ps t f = foldQueryMS pa ps t $ lmap productTypeTo f

data DecodingError l
  = DecodingErrorJSOP JSOPIssue
  | DecodingErrorParsing l
  deriving (Show, Eq, GHC.Generic, Exception)

queryStreamingAny ::
  ( KnownSymbol k,
    IsProductType b xs,
    All Typeable xs
  ) =>
  p k ->
  NP (JSOP Key) xs ->
  Text ->
  (b -> Either l a) ->
  Stream (Of (Either (DecodingError l) a)) WithSF ()
queryStreamingAny object decoding filters action =
  S.map
    do either (Left . DecodingErrorJSOP) (left DecodingErrorParsing . action . productTypeTo)
    do
      queryStreamingS
        do object
        do decoding
        do filters

logLefts :: MonadIO m => Logger l -> Stream (Of (Either l a)) m r -> Stream (Of a) m r
logLefts logger = S.mapM_ logger . S.separate . S.maps S.eitherToSum

queryStreamingAnyL ::
  (KnownSymbol k, All Typeable xs, IsProductType b xs) =>
  Logger (DecodingError l) ->
  p k ->
  NP (JSOP Key) xs ->
  Text ->
  (b -> Either l a) ->
  Stream (Of a) WithSF ()
queryStreamingAnyL logger object decoding filters =
  logLefts logger
    . queryStreamingAny object decoding filters