{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# OPTIONS_GHC -Wall #-}

module Salesforce.API where

import Control.Lens
    ( Traversable(traverse),
      (^..),
      (^?),
      iso,
      makeWrapped,
      _Unwrapped,
      Iso',
      Choice )
import Control.Logging (errorL', warnS')
import Data.Aeson (Value (..), object, Key)
import Data.Aeson.Key (toText)
import Data.Aeson.Lens ( key, AsValue(_Array, _String) )
import Data.Aeson.Types (Pair)
import Data.Csv (FromField, ToField)
import Data.JSOP ( jread, ksplit, paths, JSOP, JSOPIssue )
import qualified Data.Text as T
import Data.TreeDiff (ToExpr)
import qualified Data.Vector as V
import Generics.SOP (All, IsProductType, NP)
import Lib.Retry ( rt )
import Streaming (Stream, Of)
import qualified Streaming.Prelude as S
import Protolude
    ( ($),
      Eq((==)),
      Ord,
      Typeable,
      IsString,
      Applicative(pure),
      Foldable(foldr),
      Generic,
      KnownSymbol,
      Semigroup((<>)),
      Maybe(Just, Nothing),
      Either,
      Symbol,
      (<$>),
      void,
      (.),
      symbolVal,
      identity,
      map,
      show,
      either,
      Proxy(..),
      NFData,
      ConvertText(toS),
      Text,
      lift )
import Protolude.Unsafe (unsafeLast)
import Salesforce.Login
import Servant.API
  ( Capture
  , Get
  , Header
  , JSON
  , QueryParam
  , ReqBody
  , (:>), DeleteNoContent, PostCreated, PatchNoContent
  )
import Servant.Client
import qualified Prelude
import Data.Aeson.QQ
-- import Text.InterpolatedString.QM (qmb)

--------------------------------------------------------------------------------
-- getting a token
--------------------------------------------------------------------------------
handshake :: Value
handshake =
      [aesonQQ| 
        { version: 1.0
        , minimumVersion: 1.0
        , channel: "/meta/handshake"
        , supportedConnectionTypes: ["long-polling"]
        }
      |]

type CDCApi a =
  Header "Authorization" Text :> "cometd" :> "47.0" :> a

type ChangeAll = CDCApi (ReqBody '[JSON] Value :> PostCreated '[JSON] Value)

changeAll :: Proxy ChangeAll
changeAll = Proxy

-- getChanges :: _
getChanges :: WithSF Value
getChanges = withToken $ \tk -> client changeAll tk $ Array $ V.fromList [handshake]

type DataApi a =
  Header "Authorization" Text :> "services" :> "data" :> "v53.0" :> a

type QueryApi = DataApi ("query" :> QueryParam "q" Text :> Get '[JSON] Value)

type QueryRestApi = DataApi ("query" :> Capture "id" Text :> Get '[JSON] Value)

queryRestP :: Proxy QueryRestApi
queryRestP = Proxy

queryP :: Proxy QueryApi
queryP = Proxy

querySF :: Text -> WithSF [Value]
querySF v = rt $ getAllRecords $ withToken $ \tk -> client queryP tk $ Just v

modifySFP
  :: forall a p b xs.
  (KnownSymbol a, IsProductType b xs, All Typeable xs)
  => p a
  -> NP (JSOP Key) xs
  -> (b -> WithSF ())
  -> WithSF ()
modifySFP object' parsers effect
    = S.mapM_ (either warnParseFailure effect . parseQueryResult)
    $ querySFP query
  where
    query :: Text
    query = gqueryX (toS $ symbolVal object') (paths parsers) ""
    parseQueryResult :: Value -> Either JSOPIssue b
    parseQueryResult = jread (ksplit ".") parsers
    warnParseFailure :: JSOPIssue -> WithSF ()
    warnParseFailure = warnS' "parsing json" . show

gqueryX :: Text -> [Key] -> Text -> Text
gqueryX object' fields cond =
  "Select "
    <> T.intercalate ", " (map toText fields)
    <> " from "
    <> object'
    <> " "
    <> cond

querySFP :: Text -> Stream (Of Value) WithSF ()
querySFP v = getAllRecordsP $ withToken $ \tk -> client queryP tk $ Just v

lastSegment :: Text -> Text
lastSegment = unsafeLast . T.split (== '/')

getAllRecords :: WithSF Value -> WithSF [Value]
getAllRecords f = do
  r <- f
  let rs = r ^.. key "records" . _Array . traverse
      mc = r ^? key "nextRecordsUrl" . _String
  case mc of
    Nothing -> pure rs
    Just c ->
      (rs <>) <$> getAllRecords do
        rt $ withToken $ \tk -> client queryRestP tk $ lastSegment c

getAllRecordsP :: WithSF Value -> Stream (Of Value) WithSF ()
getAllRecordsP f = do
  let loop f' = do
        r <- lift f'
        S.each $ r ^.. key "records" . _Array . traverse
        case r ^? key "nextRecordsUrl" . _String of
          Nothing -> pure ()
          Just c -> loop do
            rt $ withToken $ \tk -> client queryRestP tk $ lastSegment c
  loop f

--------------------------------------------------------------------------------
-- post/patch monthly usage
--------------------------------------------------------------------------------

type ModApi (a :: Symbol) b = DataApi ("sobjects" :> a :> b)

type PostApi (a :: Symbol) =
  ModApi a (ReqBody '[JSON] Value :> PostCreated '[JSON] Value)

type PatchApi (a :: Symbol) =
  ModApi
    a
    ( Capture "id" Text
        :> ReqBody '[JSON] Value
        :> PatchNoContent
    )

type DeleteApi (a :: Symbol) =
  ModApi a (Capture "id" Text :> DeleteNoContent)

--------------------------------------------------------------------------------
-- class
--------------------------------------------------------------------------------


newtype IndexOf' k (s :: Symbol) = IndexOf {indexOf :: Text}
  deriving (IsString, Eq, Ord, Generic, ToExpr, NFData, ToField, FromField)

makeWrapped ''IndexOf' 

type IndexOf = IndexOf' () 

type OrderItemO = "OrderItem"

instance KnownSymbol s => Prelude.Show (IndexOf s) where
  show idx@(IndexOf x) = symbolVal idx <> ": " <> toS x

data SFI (x :: Symbol)  
instance (KnownSymbol s, KnownSymbol url) => Prelude.Show (IndexOf' (SFI url) s) where
  show (IndexOf x) =  symbolVal (Proxy @url) <> toS x

mkIndex' :: forall s k . Text -> IndexOf' k s 
mkIndex' = IndexOf

mkIndex :: p s -> Text -> IndexOf' k s
mkIndex _ = IndexOf

indexP :: p s -> Iso' Text (IndexOf' k s)
indexP p = iso (mkIndex p) indexOf

-- sfIndex :: forall s. Iso' Value (IndexOf s)
sfIndex
  :: forall s t p k f.
  (AsValue t, Choice p, Applicative f)
  => p (IndexOf' k s) (f (IndexOf' k s))
  -> p t (f t)
sfIndex = _String . _Unwrapped

class DeleteC p a where
  deleteC :: p a -> Proxy (DeleteApi a)

instance DeleteC p a where
  deleteC _ = Proxy

class PatchC p a where
  patchC :: p a -> Proxy (PatchApi a)

instance PatchC p a where
  patchC _ = Proxy

class PostC a where
  postC :: p a -> Proxy (PostApi a)

instance PostC a where
  postC _ = Proxy

postValue :: KnownSymbol a => p a -> Value -> WithSF (IndexOf' k a)
postValue o x = do
  v <- rt $ withToken $ \tk -> client (postC o) tk x
  case v ^? key "id" . _String of
    Nothing -> errorL' "post failed"
    Just id -> pure $ mkIndex o id

post :: KnownSymbol a => p a -> [Pair] -> WithSF (IndexOf' k a)
post o = postValue o . object

delete :: KnownSymbol a => IndexOf' k a -> WithSF ()
delete o@(IndexOf id) = void $ rt $ withToken $ \tk -> client (deleteC o) tk id

patchValue :: KnownSymbol a => IndexOf' k a -> Value -> WithSF ()
patchValue o@(IndexOf id) x = void $ rt $ withToken $ \tk -> client (patchC o) tk id x

patch :: KnownSymbol a => IndexOf' k a -> [Pair] -> WithSF ()
patch o = patchValue o . object

-- famous
type PriceBookO = "Pricebook2"


type PriceBookEntryO = "PricebookEntry"



keys :: Applicative f => Key -> (Value -> f Value) -> Value -> f Value
keys = foldr  (\k -> (.) (key k) ) identity . ksplit "."
