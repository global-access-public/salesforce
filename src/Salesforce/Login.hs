{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wall #-}

module Salesforce.Login
  ( CallSF
  , CallSFT (..)
  , LoginApi
  , OauthApi
  , SfConf (..)
  , SfCreds (..)
  , SfEnvironment (..)
  , Token
  , WithSF
  , WithToken
  , getSFConfig
  , callSFConf
  , callSFY
  , clientEnv
  , getToken
  , loginP
  , mkCreds
  , runEnvClientM
  , sfEnv
  , withSF
  , withSFY
  , withToken
  )
where

import Control.Lens ((^?))
import Control.Monad.Catch (throwM)
import Data.Aeson.Lens (_String, key)
import Data.FFunctor (FFunctor (..))
import Data.Yaml (FromJSON, Value, decodeFileThrow)
import Data.Text qualified as Text
import Deriving.Aeson
  ( CamelToSnake
  , CustomJSON (CustomJSON)
  , FieldLabelModifier
  , StripPrefix
  )
import Network.HTTP.Client (ManagerSettings (managerResponseTimeout), newManager, responseTimeoutMicro)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Protolude hiding (to)
import Servant.API (FormUrlEncoded, JSON, Post, ReqBody, (:>))
import Servant.Client
  ( BaseUrl
  , ClientEnv
  , ClientM
  , client
  , mkClientEnv
  , showBaseUrl
  , BaseUrl (..)
  , runClientM
  )

--------------------------------------------------------------------------------
-- getting a token
--------------------------------------------------------------------------------
type Token = Text

type WithToken = ReaderT Token

type WithSF = WithToken ClientM

newtype CallSFT m = CallSFT
  {runSF :: forall a. WithSF a -> m a}

instance FFunctor CallSFT where
  ffmap f (CallSFT g) = CallSFT $ f . g

type CallSF = CallSFT IO

clientEnv :: BaseUrl -> IO ClientEnv
clientEnv bu = do
  manager <- newManager tlsManagerSettings {managerResponseTimeout = responseTimeoutMicro $ 120 * 1_000_000}
  pure $ mkClientEnv manager bu

runEnvClientM :: BaseUrl -> ClientM a -> IO a
runEnvClientM bu c = do
  ce <- clientEnv bu
  r <- runClientM c ce
  case r of
    Left e -> throwM e
    Right x -> pure x

getSFConfig :: MonadIO m => FilePath -> m SfConf
getSFConfig = liftIO . decodeFileThrow

callSFY :: MonadIO m => FilePath -> m (CallSFT m)
callSFY c = getSFConfig c >>= callSFConf

callSFConf :: MonadIO m => SfConf -> m (CallSFT m)
callSFConf (SfConf bu cs) = liftIO $ do
  tk <- runEnvClientM bu $ getToken $ mkCreds cs
  pure $ CallSFT $ \f -> liftIO $ runEnvClientM bu $ runReaderT f tk

withSF :: MonadIO m => (FilePath -> m (CallSFT m)) -> FilePath -> WithSF b -> m b
withSF call fp f = do
  CallSFT sf <- call fp
  sf f

withSFY :: MonadIO m => FilePath -> WithSF b -> m b
withSFY = withSF callSFY

withToken :: (Maybe Token -> ClientM a) -> WithSF a
withToken f = ask >>= lift . f . Just

type OauthApi a = "services" :> "oauth2" :> "token" :> a

type LoginApi =
  OauthApi (ReqBody '[FormUrlEncoded] [(Text, Text)] :> Post '[JSON] Value)

loginP :: Proxy LoginApi
loginP = Proxy

getToken :: [(Text, Text)] -> ClientM Token
getToken cs = do
  r <- client loginP cs
  case r ^? key "access_token" . _String of
    Nothing -> panic "no token"
    Just txt -> pure ("Bearer " <> txt)

newtype SfEnvironment = MkSfEnvironment{ sfEnv_text :: Text }
  deriving (Eq, Ord, Show, IsString)

sfEnv :: SfConf -> SfEnvironment
sfEnv SfConf{ sfUrl = url } = MkSfEnvironment . fromMaybe ("unknown:" <> Text.pack (showBaseUrl url)) . parseSfEnvFromUrl $ url

parseSfEnvFromUrl :: BaseUrl -> Maybe Text
parseSfEnvFromUrl url = do
  part <- Text.stripSuffix ".my.salesforce.com" . Text.pack $ baseUrlHost url
  let (hasSandboxSuffix, name) = case Text.stripSuffix ".sandbox" part of
        Just x  -> (True, x)
        Nothing -> (False, part)
  let category | hasSandboxSuffix || "--" `Text.isInfixOf` name = "sandbox"
               | '.' `Text.elem` name = "unknown"
               | otherwise = "production"
  pure (category <> ":" <> name)

data SfConf = SfConf
  { sfUrl :: BaseUrl
  , sfCreds :: SfCreds
  }
  deriving (Show, Eq, Generic)
  deriving
    (FromJSON)
    via CustomJSON '[FieldLabelModifier '[CamelToSnake, StripPrefix "sf_"]] SfConf

data SfCreds = SfCreds
  { sfUsername :: Text
  , sfPassword :: Text
  , sfClientId :: Text
  , sfClientSecret :: Text
  }
  deriving (Show, Eq, Generic)
  deriving
    (FromJSON)
    via CustomJSON '[FieldLabelModifier '[CamelToSnake, StripPrefix "sf_"]] SfCreds

mkCreds :: SfCreds -> [(Text, Text)]
mkCreds SfCreds {..} =
  [ ("username", sfUsername)
  , ("password", sfPassword)
  , ("grant_type", "password")
  , ("client_id", sfClientId)
  , ("client_secret", sfClientSecret)
  ]
