{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Salesforce.Generic where

-- import Data.Semigroup (First (..))

import Data.JSOP
import Data.Aeson.Key (Key, toText)
import qualified Data.Text as T
import Generics.SOP (All, IsProductType, NP, Top)
import Protolude hiding (All, to, (:*:))
import Salesforce.API
import Salesforce.Login
import Streaming (Stream, Of)
import qualified Streaming.Prelude as S

-- | build a query out of fields
gqueryQ :: Text -> [Key] -> Text -> Text
gqueryQ object' fields cond =
  "Select "
    <> T.intercalate ", " (map toText fields)
    <> " from "
    <> object'
    <> " "
    <> cond

gquerySOP
  :: (All Top xs, KnownSymbol a)
  => p a
  -> NP (JSOP Key) xs
  -> Text
  -> Text
gquerySOP object' columns' =
  gqueryQ
    (toS $ symbolVal object')
    (paths columns')

parse
  :: (All Typeable xs, KnownSymbol a, IsProductType b xs)
  => p a
  -> NP (JSOP Key) xs
  -> Text
  -> WithSF (Either JSOPIssue [b])
parse object' query cond = do
  rs <- querySF $ gquerySOP object' query cond
  pure $ sequence $ jread (ksplit ".") query <$> rs


parseP
  :: (All Typeable xs, KnownSymbol a, IsProductType b xs)
  => p a
  -> NP (JSOP Key) xs
  -> Text
  -> Stream (Of (Either JSOPIssue b)) WithSF ()
parseP object' query cond
  = S.map (jread (ksplit ".") query) 
  $ querySFP (gquerySOP object' query cond)